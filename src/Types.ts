export type AnswerType = {
    text: string;
    correct: boolean;
};
export type QuestionType = {
    id: number;
    text: string;
    answers: AnswerType[];
    tags: string[];
    explanation: string;
    chosenAnswer: null | number;
};
