Tools to absorb
 - UWorld
 - Cramfighter

Ideas:


# Wards Wisdom Online

## Purpose
Knowledge ~~that helps teach people to save lives~~ should be free.

### Question list format
Currently, available questions are pulled in as a flat json file that includes question text and any formatting information, as well as annotation for correct answer choices, categories/tags, etc. Rough schema below:

```json
[
  {
    "id": 223,
    "question": "Why did the Chicken cross the road?",
    "correctAnswer": "To get to the Other Side",
    "wrongAnswers": [
      "To escape the Grapes of Wrath",
      "Because I said so",
      "Green Eggs and Ham",
    ],
    "tags": [
      "existential",
      "psychology"
    ],
  },
  ...
]
```

This is likely to change in the future as more questions and multimedia are added. We can only hold so much in memory.

## Timeline
 - MVP: static question bank with saved history
   - [x] Load question list
   - [x] Generate test
   - [x] Pick test questions from subjects
   - [x] View and answer questions sequentially
   - [x] Explain answers
   - [x] Summary at end of test
   - [ ] Save each test locally for posterity
 - [ ] Performance Analysis
   - [ ] Question swapping good vs. bad
   - [ ] Topics you do well/poorly on
 - [ ] Sync History between devices
 - [ ] Other criteria for picking questions to go in a test
 - [ ] Download History
 - [ ] Backend stats collection for comparison (and question pruning)
 - [ ] True Practice Test simulation
 - [ ] Integrated Question Submission/Review
 - [ ] Per-answer explanations
 - [ ] flexible questions that allow multiple portions of the question to be tested
 - [ ] Scheduling tools a la cramfighter/anki
 - [ ] auto-generated flashcards?
